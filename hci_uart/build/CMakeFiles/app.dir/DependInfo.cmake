# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/src/main.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/CMakeFiles/app.dir/src/main.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "BUILD_VERSION=zephyr-v1.13.0-1877-g9d14874db1"
  "DEVELOP_IN_NRF52832"
  "KERNEL"
  "NRF52810_XXAA"
  "_FORTIFY_SOURCE=2"
  "__ZEPHYR__=1"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/akeane/dev/ic8/github/zephyr/kernel/include"
  "/home/akeane/dev/ic8/github/zephyr/arch/arm/include"
  "/home/akeane/dev/ic8/github/zephyr/soc/arm/nordic_nrf/nrf52"
  "/home/akeane/dev/ic8/github/zephyr/soc/arm/nordic_nrf/nrf52/include"
  "/home/akeane/dev/ic8/github/zephyr/soc/arm/nordic_nrf/include"
  "/home/akeane/dev/ic8/github/zephyr/boards/arm/nrf52810_pca10040"
  "/home/akeane/dev/ic8/github/zephyr/include"
  "/home/akeane/dev/ic8/github/zephyr/include/drivers"
  "zephyr/include/generated"
  "/usr/lib/gcc/arm-none-eabi/6.3.1/include"
  "/usr/lib/gcc/arm-none-eabi/6.3.1/include-fixed"
  "/home/akeane/dev/ic8/github/zephyr/lib/libc/minimal/include"
  "/home/akeane/dev/ic8/github/zephyr/ext/lib/crypto/tinycrypt/include"
  "/home/akeane/dev/ic8/github/zephyr/ext/hal/cmsis/Include"
  "/home/akeane/dev/ic8/github/zephyr/ext/hal/nordic/nrfx"
  "/home/akeane/dev/ic8/github/zephyr/ext/hal/nordic/nrfx/drivers/include"
  "/home/akeane/dev/ic8/github/zephyr/ext/hal/nordic/nrfx/hal"
  "/home/akeane/dev/ic8/github/zephyr/ext/hal/nordic/nrfx/mdk"
  "/home/akeane/dev/ic8/github/zephyr/ext/hal/nordic/."
  "/home/akeane/dev/ic8/github/zephyr/subsys/bluetooth"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

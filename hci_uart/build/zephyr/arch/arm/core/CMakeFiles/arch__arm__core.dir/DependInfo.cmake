# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "ASM"
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_ASM
  "/home/akeane/dev/ic8/github/zephyr/arch/arm/core/cpu_idle.S" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/arch/arm/core/CMakeFiles/arch__arm__core.dir/cpu_idle.S.obj"
  "/home/akeane/dev/ic8/github/zephyr/arch/arm/core/exc_exit.S" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/arch/arm/core/CMakeFiles/arch__arm__core.dir/exc_exit.S.obj"
  "/home/akeane/dev/ic8/github/zephyr/arch/arm/core/fault_s.S" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/arch/arm/core/CMakeFiles/arch__arm__core.dir/fault_s.S.obj"
  "/home/akeane/dev/ic8/github/zephyr/arch/arm/core/isr_wrapper.S" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/arch/arm/core/CMakeFiles/arch__arm__core.dir/isr_wrapper.S.obj"
  "/home/akeane/dev/ic8/github/zephyr/arch/arm/core/swap_helper.S" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/arch/arm/core/CMakeFiles/arch__arm__core.dir/swap_helper.S.obj"
  )
set(CMAKE_ASM_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_ASM
  "BUILD_VERSION=zephyr-v1.13.0-1877-g9d14874db1"
  "DEVELOP_IN_NRF52832"
  "KERNEL"
  "NRF52810_XXAA"
  "_FORTIFY_SOURCE=2"
  "__ZEPHYR_SUPERVISOR__"
  "__ZEPHYR__=1"
  )

# The include file search paths:
set(CMAKE_ASM_TARGET_INCLUDE_PATH
  "../../../../kernel/include"
  "../../../../arch/arm/include"
  "../../../../soc/arm/nordic_nrf/nrf52"
  "../../../../soc/arm/nordic_nrf/nrf52/include"
  "../../../../soc/arm/nordic_nrf/include"
  "../../../../boards/arm/nrf52810_pca10040"
  "../../../../include"
  "../../../../include/drivers"
  "zephyr/include/generated"
  "/usr/lib/gcc/arm-none-eabi/6.3.1/include"
  "/usr/lib/gcc/arm-none-eabi/6.3.1/include-fixed"
  "../../../../lib/libc/minimal/include"
  "../../../../ext/lib/crypto/tinycrypt/include"
  "../../../../ext/hal/cmsis/Include"
  "../../../../ext/hal/nordic/nrfx"
  "../../../../ext/hal/nordic/nrfx/drivers/include"
  "../../../../ext/hal/nordic/nrfx/hal"
  "../../../../ext/hal/nordic/nrfx/mdk"
  "../../../../ext/hal/nordic/."
  "../../../../subsys/bluetooth"
  )
set(CMAKE_DEPENDS_CHECK_C
  "/home/akeane/dev/ic8/github/zephyr/arch/arm/core/fatal.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/arch/arm/core/CMakeFiles/arch__arm__core.dir/fatal.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/arch/arm/core/fault.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/arch/arm/core/CMakeFiles/arch__arm__core.dir/fault.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/arch/arm/core/irq_init.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/arch/arm/core/CMakeFiles/arch__arm__core.dir/irq_init.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/arch/arm/core/irq_manage.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/arch/arm/core/CMakeFiles/arch__arm__core.dir/irq_manage.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/arch/arm/core/swap.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/arch/arm/core/CMakeFiles/arch__arm__core.dir/swap.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/arch/arm/core/sys_fatal_error_handler.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/arch/arm/core/CMakeFiles/arch__arm__core.dir/sys_fatal_error_handler.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/arch/arm/core/thread.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/arch/arm/core/CMakeFiles/arch__arm__core.dir/thread.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/arch/arm/core/thread_abort.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/arch/arm/core/CMakeFiles/arch__arm__core.dir/thread_abort.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "BUILD_VERSION=zephyr-v1.13.0-1877-g9d14874db1"
  "DEVELOP_IN_NRF52832"
  "KERNEL"
  "NRF52810_XXAA"
  "_FORTIFY_SOURCE=2"
  "__ZEPHYR_SUPERVISOR__"
  "__ZEPHYR__=1"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../../../../kernel/include"
  "../../../../arch/arm/include"
  "../../../../soc/arm/nordic_nrf/nrf52"
  "../../../../soc/arm/nordic_nrf/nrf52/include"
  "../../../../soc/arm/nordic_nrf/include"
  "../../../../boards/arm/nrf52810_pca10040"
  "../../../../include"
  "../../../../include/drivers"
  "zephyr/include/generated"
  "/usr/lib/gcc/arm-none-eabi/6.3.1/include"
  "/usr/lib/gcc/arm-none-eabi/6.3.1/include-fixed"
  "../../../../lib/libc/minimal/include"
  "../../../../ext/lib/crypto/tinycrypt/include"
  "../../../../ext/hal/cmsis/Include"
  "../../../../ext/hal/nordic/nrfx"
  "../../../../ext/hal/nordic/nrfx/drivers/include"
  "../../../../ext/hal/nordic/nrfx/hal"
  "../../../../ext/hal/nordic/nrfx/mdk"
  "../../../../ext/hal/nordic/."
  "../../../../subsys/bluetooth"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

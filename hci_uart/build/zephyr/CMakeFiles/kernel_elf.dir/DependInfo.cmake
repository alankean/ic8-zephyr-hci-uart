# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/isr_tables.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/CMakeFiles/kernel_elf.dir/isr_tables.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/misc/empty_file.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/CMakeFiles/kernel_elf.dir/misc/empty_file.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "BUILD_VERSION=zephyr-v1.13.0-1877-g9d14874db1"
  "DEVELOP_IN_NRF52832"
  "KERNEL"
  "NRF52810_XXAA"
  "_FORTIFY_SOURCE=2"
  "__ZEPHYR__=1"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../../../../kernel/include"
  "../../../../arch/arm/include"
  "../../../../soc/arm/nordic_nrf/nrf52"
  "../../../../soc/arm/nordic_nrf/nrf52/include"
  "../../../../soc/arm/nordic_nrf/include"
  "../../../../boards/arm/nrf52810_pca10040"
  "../../../../include"
  "../../../../include/drivers"
  "zephyr/include/generated"
  "/usr/lib/gcc/arm-none-eabi/6.3.1/include"
  "/usr/lib/gcc/arm-none-eabi/6.3.1/include-fixed"
  "../../../../lib/libc/minimal/include"
  "../../../../ext/lib/crypto/tinycrypt/include"
  "../../../../ext/hal/cmsis/Include"
  "../../../../ext/hal/nordic/nrfx"
  "../../../../ext/hal/nordic/nrfx/drivers/include"
  "../../../../ext/hal/nordic/nrfx/hal"
  "../../../../ext/hal/nordic/nrfx/mdk"
  "../../../../ext/hal/nordic/."
  "../../../../subsys/bluetooth"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/CMakeFiles/app.dir/DependInfo.cmake"
  "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/CMakeFiles/zephyr.dir/DependInfo.cmake"
  "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/arch/arm/core/CMakeFiles/arch__arm__core.dir/DependInfo.cmake"
  "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/arch/arm/core/cortex_m/CMakeFiles/arch__arm__core__cortex_m.dir/DependInfo.cmake"
  "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/arch/arm/core/cortex_m/mpu/CMakeFiles/arch__arm__core__cortex_m__mpu.dir/DependInfo.cmake"
  "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/lib/libc/minimal/CMakeFiles/lib__libc__minimal.dir/DependInfo.cmake"
  "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/subsys/bluetooth/common/CMakeFiles/subsys__bluetooth__common.dir/DependInfo.cmake"
  "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/subsys/bluetooth/host/CMakeFiles/subsys__bluetooth__host.dir/DependInfo.cmake"
  "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/subsys/bluetooth/controller/CMakeFiles/subsys__bluetooth__controller.dir/DependInfo.cmake"
  "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/subsys/net/CMakeFiles/subsys__net.dir/DependInfo.cmake"
  "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/drivers/gpio/CMakeFiles/drivers__gpio.dir/DependInfo.cmake"
  "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/drivers/serial/CMakeFiles/drivers__serial.dir/DependInfo.cmake"
  "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/drivers/entropy/CMakeFiles/drivers__entropy.dir/DependInfo.cmake"
  "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/kernel/CMakeFiles/kernel.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

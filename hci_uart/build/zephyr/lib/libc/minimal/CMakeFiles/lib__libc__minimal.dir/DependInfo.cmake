# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/akeane/dev/ic8/github/zephyr/lib/libc/minimal/source/stdlib/atoi.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/lib/libc/minimal/CMakeFiles/lib__libc__minimal.dir/source/stdlib/atoi.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/lib/libc/minimal/source/stdlib/malloc.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/lib/libc/minimal/CMakeFiles/lib__libc__minimal.dir/source/stdlib/malloc.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/lib/libc/minimal/source/stdlib/strtol.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/lib/libc/minimal/CMakeFiles/lib__libc__minimal.dir/source/stdlib/strtol.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/lib/libc/minimal/source/stdlib/strtoul.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/lib/libc/minimal/CMakeFiles/lib__libc__minimal.dir/source/stdlib/strtoul.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/lib/libc/minimal/source/stdout/fprintf.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/lib/libc/minimal/CMakeFiles/lib__libc__minimal.dir/source/stdout/fprintf.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/lib/libc/minimal/source/stdout/prf.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/lib/libc/minimal/CMakeFiles/lib__libc__minimal.dir/source/stdout/prf.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/lib/libc/minimal/source/stdout/sprintf.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/lib/libc/minimal/CMakeFiles/lib__libc__minimal.dir/source/stdout/sprintf.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/lib/libc/minimal/source/stdout/stdout_console.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/lib/libc/minimal/CMakeFiles/lib__libc__minimal.dir/source/stdout/stdout_console.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/lib/libc/minimal/source/string/string.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/lib/libc/minimal/CMakeFiles/lib__libc__minimal.dir/source/string/string.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/lib/libc/minimal/source/string/strncasecmp.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/lib/libc/minimal/CMakeFiles/lib__libc__minimal.dir/source/string/strncasecmp.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/lib/libc/minimal/source/string/strstr.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/lib/libc/minimal/CMakeFiles/lib__libc__minimal.dir/source/string/strstr.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "BUILD_VERSION=zephyr-v1.13.0-1877-g9d14874db1"
  "DEVELOP_IN_NRF52832"
  "KERNEL"
  "NRF52810_XXAA"
  "_FORTIFY_SOURCE=2"
  "__ZEPHYR__=1"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../../../../kernel/include"
  "../../../../arch/arm/include"
  "../../../../soc/arm/nordic_nrf/nrf52"
  "../../../../soc/arm/nordic_nrf/nrf52/include"
  "../../../../soc/arm/nordic_nrf/include"
  "../../../../boards/arm/nrf52810_pca10040"
  "../../../../include"
  "../../../../include/drivers"
  "zephyr/include/generated"
  "/usr/lib/gcc/arm-none-eabi/6.3.1/include"
  "/usr/lib/gcc/arm-none-eabi/6.3.1/include-fixed"
  "../../../../lib/libc/minimal/include"
  "../../../../ext/lib/crypto/tinycrypt/include"
  "../../../../ext/hal/cmsis/Include"
  "../../../../ext/hal/nordic/nrfx"
  "../../../../ext/hal/nordic/nrfx/drivers/include"
  "../../../../ext/hal/nordic/nrfx/hal"
  "../../../../ext/hal/nordic/nrfx/mdk"
  "../../../../ext/hal/nordic/."
  "../../../../subsys/bluetooth"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

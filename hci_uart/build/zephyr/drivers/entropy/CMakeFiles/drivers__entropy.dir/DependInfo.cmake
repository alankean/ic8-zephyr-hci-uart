# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/akeane/dev/ic8/github/zephyr/drivers/entropy/entropy_nrf5.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/drivers/entropy/CMakeFiles/drivers__entropy.dir/entropy_nrf5.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "BUILD_VERSION=zephyr-v1.13.0-1877-g9d14874db1"
  "DEVELOP_IN_NRF52832"
  "KERNEL"
  "NRF52810_XXAA"
  "_FORTIFY_SOURCE=2"
  "__ZEPHYR_SUPERVISOR__"
  "__ZEPHYR__=1"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../../../../kernel/include"
  "../../../../arch/arm/include"
  "../../../../soc/arm/nordic_nrf/nrf52"
  "../../../../soc/arm/nordic_nrf/nrf52/include"
  "../../../../soc/arm/nordic_nrf/include"
  "../../../../boards/arm/nrf52810_pca10040"
  "../../../../include"
  "../../../../include/drivers"
  "zephyr/include/generated"
  "/usr/lib/gcc/arm-none-eabi/6.3.1/include"
  "/usr/lib/gcc/arm-none-eabi/6.3.1/include-fixed"
  "../../../../lib/libc/minimal/include"
  "../../../../ext/lib/crypto/tinycrypt/include"
  "../../../../ext/hal/cmsis/Include"
  "../../../../ext/hal/nordic/nrfx"
  "../../../../ext/hal/nordic/nrfx/drivers/include"
  "../../../../ext/hal/nordic/nrfx/hal"
  "../../../../ext/hal/nordic/nrfx/mdk"
  "../../../../ext/hal/nordic/."
  "../../../../subsys/bluetooth"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

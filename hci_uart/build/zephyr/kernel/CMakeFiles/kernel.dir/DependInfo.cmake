# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/akeane/dev/ic8/github/zephyr/kernel/alert.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/kernel/CMakeFiles/kernel.dir/alert.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/kernel/device.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/kernel/CMakeFiles/kernel.dir/device.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/kernel/errno.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/kernel/CMakeFiles/kernel.dir/errno.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/kernel/idle.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/kernel/CMakeFiles/kernel.dir/idle.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/kernel/init.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/kernel/CMakeFiles/kernel.dir/init.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/kernel/mailbox.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/kernel/CMakeFiles/kernel.dir/mailbox.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/kernel/mem_slab.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/kernel/CMakeFiles/kernel.dir/mem_slab.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/kernel/mempool.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/kernel/CMakeFiles/kernel.dir/mempool.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/kernel/msg_q.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/kernel/CMakeFiles/kernel.dir/msg_q.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/kernel/mutex.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/kernel/CMakeFiles/kernel.dir/mutex.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/kernel/pipes.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/kernel/CMakeFiles/kernel.dir/pipes.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/kernel/poll.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/kernel/CMakeFiles/kernel.dir/poll.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/kernel/queue.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/kernel/CMakeFiles/kernel.dir/queue.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/kernel/sched.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/kernel/CMakeFiles/kernel.dir/sched.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/kernel/sem.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/kernel/CMakeFiles/kernel.dir/sem.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/kernel/smp.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/kernel/CMakeFiles/kernel.dir/smp.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/kernel/stack.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/kernel/CMakeFiles/kernel.dir/stack.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/kernel/system_work_q.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/kernel/CMakeFiles/kernel.dir/system_work_q.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/kernel/thread.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/kernel/CMakeFiles/kernel.dir/thread.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/kernel/thread_abort.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/kernel/CMakeFiles/kernel.dir/thread_abort.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/kernel/timeout.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/kernel/CMakeFiles/kernel.dir/timeout.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/kernel/timer.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/kernel/CMakeFiles/kernel.dir/timer.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/kernel/version.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/kernel/CMakeFiles/kernel.dir/version.c.obj"
  "/home/akeane/dev/ic8/github/zephyr/kernel/work_q.c" "/home/akeane/dev/ic8/github/zephyr/samples/bluetooth/hci_uart/build/zephyr/kernel/CMakeFiles/kernel.dir/work_q.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "BUILD_VERSION=zephyr-v1.13.0-1877-g9d14874db1"
  "DEVELOP_IN_NRF52832"
  "KERNEL"
  "NRF52810_XXAA"
  "_FORTIFY_SOURCE=2"
  "__ZEPHYR_SUPERVISOR__"
  "__ZEPHYR__=1"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../../../../kernel/include"
  "../../../../arch/arm/include"
  "../../../../soc/arm/nordic_nrf/nrf52"
  "../../../../soc/arm/nordic_nrf/nrf52/include"
  "../../../../soc/arm/nordic_nrf/include"
  "../../../../boards/arm/nrf52810_pca10040"
  "../../../../include"
  "../../../../include/drivers"
  "zephyr/include/generated"
  "/usr/lib/gcc/arm-none-eabi/6.3.1/include"
  "/usr/lib/gcc/arm-none-eabi/6.3.1/include-fixed"
  "../../../../lib/libc/minimal/include"
  "../../../../ext/lib/crypto/tinycrypt/include"
  "../../../../ext/hal/cmsis/Include"
  "../../../../ext/hal/nordic/nrfx"
  "../../../../ext/hal/nordic/nrfx/drivers/include"
  "../../../../ext/hal/nordic/nrfx/hal"
  "../../../../ext/hal/nordic/nrfx/mdk"
  "../../../../ext/hal/nordic/."
  "../../../../subsys/bluetooth"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
